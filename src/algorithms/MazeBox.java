package algorithms;

/**
 * Mazebox represents every cell coordinate within the maze
 */
public class MazeBox implements Comparable<MazeBox> {

    public boolean isObstacle;
    public boolean isVisited;
    public boolean isAdded;
    public MazeBox previous;
    public int x;
    public int y;
    public int so_far;
    public double to_go;

    MazeBox() {
        isObstacle = true;
        isVisited = false;
        isAdded = false;
        previous = null;
    }

    MazeBox(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int compareTo(MazeBox o) {
        if (so_far + to_go > o.so_far + o.to_go) return 1;
        else if (so_far + to_go == o.so_far + o.to_go) return 0;
        else return -1;
    }

    public void setX(int x) {
        this.x = x;
    }

}
