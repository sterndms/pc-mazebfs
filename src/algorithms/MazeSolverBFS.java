package algorithms;

import java.util.ArrayList;
import java.util.Collections;

/**
 * BFS maze solver
 */
public final class MazeSolverBFS extends MazeSearch {

    private static ArrayList<MazeBox> front;
    private final boolean randomStep;

    /**
     * BFS initialization
     *
     * @param mazeInput
     * @param randomStep choose random neighboring algorithms.MazeBox
     */
    public MazeSolverBFS(int mazeInput[][], boolean randomStep) {
        super(mazeInput);
        front = new ArrayList<>();
        this.randomStep = randomStep;
        addFront(x, y);
    }

    @Override
    public boolean nextStep() {
        if (!front.isEmpty()) {
            MazeBox box;

            box = front.remove(0);

            if (box.isVisited) {
                return nextStep();
            }

            visit(box.x, box.y);

            if (isSolved()) {
                return false;
            }

            ArrayList<String> directions = new ArrayList<>();
            directions.add("east");
            directions.add("north");
            directions.add("south");
            directions.add("west");

            if (randomStep) {
                Collections.shuffle(directions);
            }

            for (int i = 0; i < 4; i++) {

                String direction = directions.get(i);

                switch (direction) {
                    case "east":
                        addFront(x + 1, y);
                        break;
                    case "north":
                        addFront(x, y - 1);
                        break;
                    case "south":
                        addFront(x, y + 1);
                        break;
                    case "west":
                        addFront(x - 1, y);
                        break;
                }
            }
            return true;
        }
        return false;
    }

    protected void addFront(int x, int y) {
        if (validPosition(x, y)) {
            if (maze[y][x].isAdded) {
                return;
            }
            maze[y][x].isAdded = true;
            if (step > 0) {
                maze[y][x].previous = maze[this.y][this.x];
            }
            front.add(maze[y][x]);
            int fsize = front.size();
            if (fsize > maxFront) {
                maxFront = fsize;
            }
        }
    }
}
