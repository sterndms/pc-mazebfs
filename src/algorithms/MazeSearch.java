package algorithms;

//import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;

/**
 * algorithms.MazeSearch solving algorithm structure
 */
public abstract class MazeSearch {

    protected int x, y; // current position
    protected int end_x, end_y; // end position
    protected MazeBox[][] maze; // the maze boxes
    protected int width, height; // maze dimensions
    protected int step; // solver step
    protected ArrayList<MazeBox> solution; // maze solution
    protected int maxFront; // max front set size

    /**
     * Create maze from input
     *
     * @param mazeInput 2D array with 0 and 1 for obstacles
     */
    MazeSearch(int[][] mazeInput) {

        x = -1;
        y = -1;
        end_x = -1;
        end_y = -1;
        maxFront = 0;
        solution = new ArrayList<>();
        step = 0;
        width = mazeInput[0].length;
        height = mazeInput.length;
        maze = new MazeBox[height][width];

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {

                maze[i][j] = new MazeBox();
                maze[i][j].isObstacle = mazeInput[i][j] == 3;
                maze[i][j].x = j;
                maze[i][j].y = i;

                // TODO: 10/12/2018 make 2 into constant
                if (mazeInput[i][j] == 1) {
                    x = j;
                    y = i;

                    // TODO: 10/12/2018 make 2 into constant
                } else if (mazeInput[i][j] == 2) {
                    end_x = j;
                    end_y = i;
                }
            }
        }
    }

    /**
     * Perform the next step of search
     *
     * @return true if step performed
     * @throws java.lang.InterruptedException
     */
    public abstract boolean nextStep();

    /**
     * Returns the number of steps
     *
     * @return number of steps
     */
    public int getSteps() {
        return step;
    }

    /**
     * Returns the max front set size
     *
     * @return max front set size
     */
    public int getMaxFront() {
        return maxFront;
    }

    /**
     * Visit algorithms.MazeBox in x, y position
     *
     * @param x algorithms.MazeBox x coordinate
     * @param y algorithms.MazeBox y coordinate
     * @return true if algorithms.MazeBox is visited
     */
    protected boolean visit(int x, int y) {

        if (!validPosition(x, y)) {
            return false;
        }

        this.x = x;
        this.y = y;
        maze[y][x].isVisited = true;
        step++;

        return true;
    }

    /**
     * If algorithms.MazeBox can be visited in x, y position
     *
     * @param x algorithms.MazeBox x coordinate
     * @param y algorithms.MazeBox y coordinate
     * @return true if algorithms.MazeBox can be visited in x, y position
     */
    protected boolean validPosition(int x, int y) {
        return x >= 0 && x < width && y >= 0 && y < height && !maze[y][x].isObstacle;
    }

    /**
     * Add algorithms.MazeBox in x, y position to front set
     *
     * @param x algorithms.MazeBox x coordinate
     * @param y algorithms.MazeBox y coordinate
     */
    protected void addFront(int x, int y) {
        // Code for GUI
    }

    /**
     * Get current maze solution
     *
     * @return ArrayList with current algorithms.MazeBox solution (algorithms.MazeBox items)
     */
    public ArrayList<MazeBox> getSolution() {

        solution.clear();

        if (step == 0)
            return null;

        MazeBox box = maze[y][x];
        int c = 0;

        while (c < 2) {

            solution.add(box);

            if (box != null)
                box = box.previous;
            if (box == null || box.previous == null)
                c++;
        }
        Collections.reverse(solution);
        return solution;
    }

    /**
     * Solve the maze
     *
     * @return
     * @throws InterruptedException
     */
    public ArrayList<MazeBox> solve() {
        while (nextStep()) {
            // continue tree search
        }
        if (isSolved()) {
            return getSolution();
        }
        return null;
    }

    /**
     * Checks if the maze is solved
     *
     * @return true if solution is found
     */
    public boolean isSolved() {
        return x == end_x && y == end_y;
    }
}
