package io;

import java.io.*;

public class MazeReader {

    public static int[][] read(String fileName, int size, int numberOfMazes) throws FileNotFoundException, IOException {

        int[][] maze = new int[size][size];

        File currentPath = new File("resources/datasets" + numberOfMazes);

        String savedGameFile = currentPath.getPath() + File.separator + fileName;
        BufferedReader reader = new BufferedReader(new FileReader(savedGameFile));
        String line = "";
        int row = 0;
        while ((line = reader.readLine()) != null) {
            String[] cols = line.split(","); //note that if you have used space as separator you have to split on " "
            int col = 0;
            for (String c : cols) {
                maze[row][col] = Integer.parseInt(c);
                col++;
            }
            row++;
        }
        reader.close();
        return maze;
    }

}
