package io;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MazeWriter {

    public static void write(int[][] maze, int size, int number) throws IOException {

        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < maze.length; i++)   // for each row
        {
            for (int j = 0; j < maze.length; j++)   // for each column
            {
                builder.append(maze[i][j] + "");    // append to the output string
                if (j < maze.length - 1)    // if this is not the last row element
                    builder.append(",");    // then add comma (if you don't like commas you can use spaces)
            }
            builder.append("\n");   //append new line at the end of the row
        }
//        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
        String fileName = "maze" + number + "_" + size + ".txt";

        File currentPath = new File("datasets");
        currentPath.mkdir(); // create directory if not exists
        File file = new File(currentPath.getPath());

        FileWriter fw = new FileWriter(file.getAbsoluteFile() + File.separator + fileName);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(builder.toString());
        bw.close();
    }
}
