import java.io.IOException;

public class Main {

    /**
     * @param args the command line arguments
     * @throws java.lang.InterruptedException
     */
    public static void main(String[] args) throws InterruptedException, IOException {

//        Solver.writeMazes();

        helper.Solver.solveMazes(3);
        helper.Solver.solveMazes(6);
        helper.Solver.solveMazes(12);
        helper.Solver.solveMazes(24);
    }
}
