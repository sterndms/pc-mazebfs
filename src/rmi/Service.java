package rmi;

import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface Service extends Remote {
    String sendMessage(String message) throws RemoteException;
    String register(String nodeName) throws RemoteException;
    ArrayList<int[][]> getData(String client) throws IOException, InterruptedException;
    List<String> getNodes() throws RemoteException;
    void setStart() throws RemoteException;
    void endTimer() throws RemoteException;
    void removeNode(String node) throws RemoteException;
}