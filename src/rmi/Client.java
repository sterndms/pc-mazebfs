package rmi;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;

import static helper.Solver.solveParallel;

public class Client {

    public static void main(String[] args) throws IOException, InterruptedException {

        // Who am I?
        String localHostname = InetAddress.getLocalHost().getHostName();
        System.out.println("This is host:" + localHostname);

        // What is the (default) host?
        String serviceHost = Server.MasterNodeName;

        // connect to the host and request the service
        Service service = Server.connect(serviceHost);
        service.register(localHostname);

        // get data chunk
        ArrayList<int[][]> data = service.getData(localHostname);

        // get the amount of registered nodes
        System.out.println("Registered nodes: " + service.getNodes().size());

        System.out.println("Waiting for clients...");

        while (service.getNodes().size() < 3) {
            // wait for clients
        }

        int threads = 12;

        System.out.println("Start executing clients with threads: " + threads);

        if (localHostname.equals("PI12")) {
            System.out.println("Starting timer...");
            service.setStart();
        }

        // solve parallel with the data chunk and set the amount of threads
        solveParallel(data, threads);

        // remove client when done
        service.removeNode(localHostname);

        // End timer when all clients are removed and done with computing
        if (service.getNodes().isEmpty()) {
            System.out.println("Ending timer...");
            service.endTimer();
        }
        System.out.println(localHostname + " client is done!");
    }
}