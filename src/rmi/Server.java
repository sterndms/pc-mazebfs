package rmi;

import helper.Solver;

import java.io.IOException;
import java.net.InetAddress;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server extends UnicastRemoteObject implements Service {

    public static final String MasterNodeName = "PI11";
    public static final String ServiceName = "SimpleMessenger";
    public static final int Port = 1199;

    private static String localHostname;
    private static List<String> nodes = new ArrayList<>();
    private List<int[][]> mazes = new ArrayList<>();
    private static long start;

    public Server() throws RemoteException {
    }

    public void setMazes(ArrayList<int[][]> mazes) {
        this.mazes = mazes;
    }

    @Override
    public void removeNode(String node) {
        this.nodes.remove(node);
    }

    public static Service connect(String host) {

        Service remoteService = null;

        try {
            System.out.println("Connecting to " + host);

            Registry r = LocateRegistry.getRegistry(host, Server.Port);
            remoteService = (Service) r.lookup(Server.ServiceName);

        } catch (Exception ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        return remoteService;
    }

    public String sendMessage(String message) throws RemoteException {
        System.out.println(message + " received at " + localHostname);
        return message + " (ECHO)";
    }

    public String register(String nodeName) throws RemoteException {
        // add the nodeName to our list of nodes
        nodes.add(nodeName);

        String msg = "Node:" + nodeName + " registered ";
        System.out.println(msg);
        return msg;
    }

    /**
     * Divide the data and send to clients
     *
     * @return
     * @throws RemoteException
     */
    @Override
    public ArrayList<int[][]> getData(String client) throws IOException, InterruptedException {

        // divide
        int chunk = this.mazes.size() / 3;

        ArrayList<int[][]> mazes = new ArrayList<>();

        int startIndex1 = 0;
        int endIndex1 = startIndex1 + chunk - 1;

        int startIndex2 = endIndex1 + 1;
        int endIndex2 = endIndex1 + chunk;

        int startIndex3 = endIndex2 + 1;
        int endIndex3 = endIndex2 + chunk;

        switch (client) {

            case "PI12": // first chunk
                List<int[][]> chunks1 = this.mazes.subList(startIndex1, endIndex1);
                mazes = new ArrayList<>(chunks1);
                break;

            case "PI13": // second chunk
                List<int[][]> chunks2 = this.mazes.subList(startIndex2, endIndex2);
                mazes = new ArrayList<>(chunks2);
                break;

            case "PI14": // third chunk
                List<int[][]> chunks3 = this.mazes.subList(startIndex3, endIndex3);
                mazes = new ArrayList<>(chunks3);
                break;
        }
        return mazes;
    }

    public List<String> getNodes() throws RemoteException {
        return nodes;
    }

    /**
     * Start the timer
     *
     * @throws RemoteException
     */
    public void setStart() throws RemoteException {
        this.start = System.currentTimeMillis();
    }

    /**
     * End timer when all clients are done
     */
    @Override
    public void endTimer() {
        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - this.start;
        System.out.println("Elapsed time: " + elapsedTime + "ms\n-------------------------------------");
    }

    public static void main(String[] args) {
        try {
            // get the hostname of this node
            localHostname = InetAddress.getLocalHost().getHostName();

            // start a new server object
            Server server = new Server();

            // start the registry service on this node
            Registry registry = LocateRegistry.createRegistry(Server.Port);

            // add binding to this server object and use a specific ServiceName to reference it
            registry.bind(Server.ServiceName, server);

            System.out.println(Server.ServiceName + " running on " + localHostname);

            int numberOfMazes = 24;
            server.setMazes(Solver.getMazesFromDirectory(numberOfMazes));
            System.out.println("Amount of mazes has been set to: " + numberOfMazes);

        } catch (Exception ex) {
            Logger.getLogger(Server.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }
}
