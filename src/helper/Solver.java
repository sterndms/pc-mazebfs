package helper;

import algorithms.MazeBox;
import algorithms.MazeGenerator;
import algorithms.MazeSolverBFS;
import io.MazeWriter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Solver {

    /**
     * Solve the mazes and return the average elapsed time
     *
     * @param numberOfMazes
     * @throws IOException
     * @throws InterruptedException
     */
    public static void solveMazes(int numberOfMazes) throws IOException, InterruptedException {
        long elapsedTime = 0;
        long elapsedTimeParallel2Threads = 0;
        long elapsedTimeParallel4Threads = 0;
        long elapsedTimeParallel6Threads = 0;
        long elapsedTimeParallel8Threads = 0;
        long elapsedTimeParallel12Threads = 0;
        long elapsedTimeParallel24Threads = 0;

        ArrayList<int[][]> mazes = getMazesFromDirectory(numberOfMazes);

        for (int number = 0; number < 5; number++) {
            elapsedTime += solveSerial(mazes);
            elapsedTimeParallel2Threads += solveParallel(mazes, 2);
            elapsedTimeParallel4Threads += solveParallel(mazes, 4);
            elapsedTimeParallel6Threads += solveParallel(mazes, 6);
            elapsedTimeParallel8Threads += solveParallel(mazes, 8);
            elapsedTimeParallel12Threads += solveParallel(mazes, 12);
            elapsedTimeParallel24Threads += solveParallel(mazes, 24);
        }

        System.out.println("-------------------------------------\nNumber of mazes in dataset: " + numberOfMazes + "\n");
        System.out.println("Serial Average Elapsed time: " + (elapsedTime / 5) + "ms\n-------------------------------------\n");
        System.out.println("2 threads Average Elapsed time: " + (elapsedTimeParallel2Threads / 5) + "ms\n-------------------------------------\n");
        System.out.println("4 threads Average Elapsed time: " + (elapsedTimeParallel4Threads / 5) + "ms\n-------------------------------------\n");
        System.out.println("6 threads Average Elapsed time: " + (elapsedTimeParallel6Threads / 5) + "ms\n-------------------------------------\n");
        System.out.println("8 threads Average Elapsed time: " + (elapsedTimeParallel8Threads / 5) + "ms\n-------------------------------------\n");
        System.out.println("12 threads Average Elapsed time: " + (elapsedTimeParallel12Threads / 5) + "ms\n-------------------------------------\n");
        System.out.println("24 threads Average Elapsed time: " + (elapsedTimeParallel24Threads / 5) + "ms\n-------------------------------------\n");
    }

    /**
     * Solve all mazes Serial
     *
     * @param mazes
     */
    public static long solveSerial(ArrayList<int[][]> mazes) {
//        System.out.println("-------------------------------------\nSerial BFS solving:");

        long startTime = System.currentTimeMillis();

        for (int[][] maze : mazes) {
            solveMaze(maze);
        }

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
//        System.out.println("Elapsed time: " + elapsedTime + "ms\n-------------------------------------\n");

        return elapsedTime;
    }

    /**
     * Create threadpool with available processors and solve all mazes parallel
     *
     * @param mazes
     * @throws InterruptedException
     */
    public static long solveParallel(ArrayList<int[][]> mazes, int threads) throws InterruptedException {

        // create thread pool with amount of threads
        ExecutorService executor = Executors.newFixedThreadPool(threads);

        System.out.println("-------------------------------------\nParallel BFS solving:");

        long startTime = System.currentTimeMillis();

        // solve all mazes parallel
        for (int[][] maze : mazes) {
            executor.execute(() -> solveMaze(maze));
        }

        // wait until all tasks are done
        executor.shutdown();
        executor.awaitTermination(1, TimeUnit.DAYS);

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("Elapsed time: " + elapsedTime + "ms\n-------------------------------------");

        return elapsedTime;
    }

    /**
     * Solve maze with BFS algorithm
     *
     * @param maze
     */
    public synchronized static void solveMaze(int[][] maze) {

//        System.out.println("start maze with length: " + maze.length);

        MazeSolverBFS solver = new MazeSolverBFS(maze, true);
        ArrayList<MazeBox> solution = solver.solve();

        if (solution != null) {
//            System.out.println("BFS - solution length: " + (solution.size() - 1) +
//                    " steps: " + solver.getSteps() +
//                    " max front set size: " + solver.getMaxFront());

        } else {
            System.out.println("no solution!");
        }
//        System.out.println("end maze with length: " + maze.length);
    }

    /**
     * Add all files found in directory to Arraylist
     *
     * @return Arraylist with mazes
     * @throws IOException
     */
    public static ArrayList<int[][]> getMazesFromDirectory(int numberOfMazes) throws IOException {

        File f = new File("resources/datasets" + numberOfMazes);
        ArrayList<File> files = new ArrayList<File>(Arrays.asList(f.listFiles()));
        ArrayList<int[][]> mazes = new ArrayList<>();

        for (File mazeF : files) {
            int mazeSize = Integer.parseInt(mazeF.getName().substring(7, mazeF.getName().length() - 4)); // get size
            int[][] maze = io.MazeReader.read(mazeF.getName(), mazeSize, numberOfMazes);
            mazes.add(maze);
        }
        return mazes;
    }

    /**
     * Write 10 datasets with different sizes
     *
     * @throws IOException
     * @throws InterruptedException
     */
    public static void writeMazes() throws IOException, InterruptedException {
        for (int number = 61; number < 181; number++) {
            writeMaze(2001, number);
        }
    }

    /**
     * Write maze to file
     *
     * @param size
     * @throws InterruptedException
     * @throws IOException
     */
    public static void writeMaze(int size, int number) throws InterruptedException, IOException {
        MazeGenerator generator = new MazeGenerator(size, size, false); // classic = outer edge enclosed by walls
        int mazeArray[][] = generator.generate();
        MazeWriter.write(mazeArray, size, number);
    }

    /**
     * Print maze
     *
     * @param maze
     */
    public static void printMaze(int[][] maze) {
        // Print generated maze
        for (int i = 0; i < maze.length; i++) {
            for (int j = 0; j < maze.length; j++) {
                System.out.print(maze[i][j]);
                if (j < maze.length - 1) System.out.print(" ");
            }
            System.out.print("\n");
        }
    }
}

