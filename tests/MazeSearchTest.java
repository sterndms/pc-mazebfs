import static org.junit.jupiter.api.Assertions.*;

import algorithms.MazeBox;
import algorithms.MazeSolverBFS;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public final class MazeSearchTest {

    MazeSolverBFS solver;
    ArrayList<MazeBox> solution;

    @BeforeEach
    public void Before() throws InterruptedException {
        int[][] maze = {
                {0, 3, 0, 0, 2},
                {0, 0, 3, 3, 0},
                {0, 3, 0, 0, 0},
                {0, 0, 0, 3, 0},
                {3, 1, 3, 0, 0},
        };

        solver = new MazeSolverBFS(maze, false);
        solution = solver.solve();
    }

    @Test
    public void getSolverStepsTest() {
        int steps = solver.getSteps();
        assertEquals(14, steps);
    }

    @Test
    public void getSolutionSize() {
        int size = solution.size();
        assertEquals(8, size);
    }

    @Test
    public void getMaxFrontSize() {
        int maxFront = solver.getMaxFront();
        assertEquals(4, maxFront);
    }


}
